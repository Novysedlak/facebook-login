<?php
/*
Plugin Name: FACEBOOK Login Plugin
Plugin URI: https://gitlab.com/Novysedlak/facebook-login
Description: Users can sign in to your blog via Facebook account
Version: 1.0.5
Author: MN
Author URI:  https://gitlab.com/Novysedlak
Textdomain: facebook-login
Domain Path: /languages
*/

if (! defined( 'ABSPATH' ) ){ die('File can not call directly !'); }

define('FACEBOOK_LOGIN_DOMAIN','facebook-login');
define('FACEBOOK_LOGIN_AJAX_URL', admin_url( 'admin-ajax.php' ));
define('FACEBOOK_LOGIN_PATH',plugin_dir_path( __FILE__ ));
define('FACEBOOK_LOGIN_URL',plugin_dir_url( __FILE__ ));
define('FACEBOOK_LOGIN_BASENAME', plugin_basename( __FILE__ ));
define('FACEBOOK_LOGIN_AUTH_PAGE','facebooklogin');
define('FACEBOOK_LOGIN_OPTION_NAME','facebook_login_option_name');

if (!class_exists('ClassFBLCore')) {
include_once(FACEBOOK_LOGIN_PATH.'includes/classes/ClassFBLCore.php');
}

if (class_exists('ClassFBLCore')) {
if (!class_exists('ClassFBLln18')) {
include_once(FACEBOOK_LOGIN_PATH.'includes/classes/ClassFBLln18.php');
}

if (!class_exists('ClassFBLFacebookLogin')) {
include_once(FACEBOOK_LOGIN_PATH.'includes/classes/ClassFBLFacebookLogin.php');
}

if (!class_exists('ClassFBLThemeMyLogin')) {
include_once(FACEBOOK_LOGIN_PATH.'includes/classes/ClassFBLThemeMyLogin.php');
}

if( is_admin() ){
if (!class_exists('ClassFBLAdminPage')) {
include_once(FACEBOOK_LOGIN_PATH.'includes/classes/ClassFBLAdminPage.php');
}
}

if (!class_exists('ClassFBLFilters')) {
include_once(FACEBOOK_LOGIN_PATH.'includes/classes/ClassFBLFilters.php');
}

}

$ClassFBLCore = new ClassFBLCore();

register_activation_hook( __FILE__, array( $ClassFBLCore, 'install' ) );

//update
include_once(FACEBOOK_LOGIN_PATH.'includes/libraries/wp-gitlab-updater/wp-gitlab-updater.php');
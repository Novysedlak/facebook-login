Plugin Name: FACEBOOK Login Plugin

Plugin URI: https://gitlab.com/Novysedlak/facebook-login

Description: Users can sign in to your blog via Facebook account

Version: 1.0.4

Author: Marian Novysedlak

Author URI:  https://gitlab.com/Novysedlak

Textdomain: facebook-login

Domain Path: /languages

![fblfoto](/uploads/a0b629ffae116b0c27c8c8d4d386801e/fblfoto.png)
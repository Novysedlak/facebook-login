=== Facebook Login ===
Contributors:
Tags: facebook, login, user, member
Donate link: 
Requires at least: 4.7
Tested up to: 5.0
Stable tag: 4.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Users can sign in to your blog via Facebook account.

== Installation ==
1. Upload "facebook-login.zip" archive file to the "/wp-content/plugins/" directory and extract it.
1. Activate the plugin through the "Plugins" menu in WordPress.

== Frequently Asked Questions ==
None.

== Screenshots ==

== Changelog ==

= 1.0.1 =
* allow autoupdate plugin from GitLab

= 1.0.2 =
* fix plugin functions

= 1.0.3 =
* fix security vulnerabilities

= 1.0.4 =
* fix GitLab updates.

= 1.0.5 =
* fix tml class.

== Upgrade Notice ==

= 1.0.1 =
* Allow autoupdate plugin from GitLab

= 1.0.2 =
* Fix plugin 

= 1.0.3 =
* Fix security vulnerabilities

= 1.0.4 =
* Fix GitLab updates.

= 1.0.5 =
* Fix tml class.
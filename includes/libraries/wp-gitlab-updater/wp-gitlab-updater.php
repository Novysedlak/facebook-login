<?php
/**
 * 
 * Documentation:
 * https://github.com/krafit/wp-gitlab-updater
 * https://florianbrinkmann.com/en/3797/update-wordpress-themes-plugins-gitlab/
*/
  
/**
 * How-to:
 * Include the file with the PluginUpdater class.
 * Init the plugin updater with the plugin base name.
 * remove_action hide menu in Settings in wp admin menu
 */

add_action( 'admin_init', 'wpgitlabupd_init', 999 );

function wpgitlabupd_init() {
require_once 'wp-gitlab-updater-master/wp-gitlab-updater.php';
new FBLUpdater(new Moenus\GitLabUpdater\UpdaterBase());
}


class FBLUpdater
{
	public $SettingsArgs = [];
	public $ClassPluginUpdater;
	public $ClassSettings;
	public $PluginResponse;

	function __construct()
	{
		$this->SettingsArgs = [
	    'slug' => 'facebook-login', 
	    'plugin_base_name' => 'facebook-login/facebook-login.php', 
	    'access_token' => 'ryPtmGRpmejQss513Hbe', 
	    'gitlab_url' => 'https://gitlab.com',
	    'repo' => 'Novysedlak/facebook-login',
		];

		$this->ClassSetting = new Moenus\GitLabUpdater\Settings();
		$this->ClassPluginUpdater = new Moenus\GitLabUpdater\PluginUpdater($this->SettingsArgs);
 
 		remove_action( is_multisite() ? 'network_admin_menu' : 'admin_menu', [ $this->ClassSettings, 'add_options_page' ]);
 		add_filter( 'plugins_api', array( $this, 'get_plugin_info' ), 10, 3 );

	}


	public function get_plugin_info( $false, $action, $response ) {
		// Check if this call API is for the right plugin
		if ( ! isset( $response->slug ) || $response->slug != 'facebook-login' ) {
			return false;
		}

		$plugin_data = get_plugin_data(FACEBOOK_LOGIN_PATH.'facebook-login.php');
		$gitlab_url = $this->SettingsArgs['gitlab_url'];
		$repo = $this->SettingsArgs['repo'];
		$access_token = $this->SettingsArgs['access_token'];
		$request = $this->fbl_fetch_tags_from_repo( $gitlab_url, $repo, $access_token );
		$response_code = wp_remote_retrieve_response_code( $request );

		if ( is_wp_error( $request ) || 200 !== $response_code ) {
				exit;
			} else {
				$glresponse = wp_remote_retrieve_body( $request );
			}

		$data = json_decode( $glresponse );

		/*$latest_version = $plugin_data['Version'];
		$lv = str_replace('.','',$latest_version);
		$next = (int)$lv + 1;
		$myString = (string)$next;
		$myArray = str_split($myString);
		$latest_version = '';
		foreach($myArray as $character){ $latest_version .= $character . "."; }
		$latest_version = substr($latest_version, 0, -1);
		*/

		$latest_version = $data[0]->name;

		$response->slug          = 'facebook-login';
		$response->plugin        = $repo;
		$response->name          = $plugin_data['Name'];
		$response->plugin_name   = $plugin_data['Name'];
		$response->version       = $latest_version;
		$response->author        = $plugin_data['Author'];
		$response->homepage      = $plugin_data['PluginURI'];
		$response->requires      = '4.9';
		$response->tested        = '5.0';
		$response->downloaded    = 0;
		$response->last_updated  = '';
		$response->sections      = array( 'description' => '<pre>'.$data[0]->message.'</pre>' );
		$response->download_link = $this->fbl_download_link( $gitlab_url, $repo, $access_token, $latest_version);
		return $response;
	}

	 function fbl_fetch_tags_from_repo( $gitlab_url, $repo, $access_token ) {
		$gitlab_url = untrailingslashit($gitlab_url);
		$repo = str_replace( '/', '%2F', $repo );
		$request_url = "$gitlab_url/api/v4/projects/$repo/repository/tags/?private_token=$access_token";
		$request     = wp_safe_remote_get( $request_url );

		return $request;
	}

	function fbl_download_link( $gitlab_url, $repo, $access_token, $latest_version) {
		$gitlab_url = untrailingslashit($gitlab_url);
		$repo = str_replace( '/', '%2F', $repo );
		$request_url = "$gitlab_url/api/v4/projects/$repo/repository/archive.zip?sha=$latest_version&private_token=$access_token";
		return $request_url;
	}
}